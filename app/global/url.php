<?php
    require_once 'config.php';
    define('PROTOCOL',(!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS'] == 'on')) ? 'https://' : 'http://');
    $G_parts = explode('/',$_SERVER['REQUEST_URI']);

    if (!in_array($_SERVER['SERVER_PORT'], [80, 443])) {
        $G_PORT = ':'.$_SERVER['SERVER_PORT'];
    } else {
        $G_PORT = '';
    }

    $G_global_url = $_SERVER['SERVER_NAME'].$G_PORT;
    $documentRoot = $_SERVER['DOCUMENT_ROOT'].'/';
    if(DOMAIN_START_PATH != 0){
        $G_global_url = $_SERVER['SERVER_NAME'].$G_PORT.'/'.$G_parts[DOMAIN_START_PATH].'/';
        $documentRoot = $_SERVER['DOCUMENT_ROOT'].'/'.$G_parts[DOMAIN_START_PATH].'/';
    }


    $G_global_url = PROTOCOL.$G_global_url.'/';

    Define("URL",$G_global_url);
    define('ROOT_PATH', $documentRoot);
    define('VERSION', 1);

    function urlFriendly($data){

        $data = urlencode(trim($data));
        $data = strtolower($data);
    
        return $data;
    
      } //urlFriendly
    $logo_name = 'logo1.png';
    ?>