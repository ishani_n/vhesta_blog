<?php

// Default OR INDEX
define("M_D_TITLE","Sri Lanka Karting Circuit Bandaragama | "); // Default page
define("M_D_DESC","Sri Lanka Karting Circuit Bandaragama | "); // Default page
define("M_D_IMG","assets/images/meta/index/web_img.png"); // Default page
define("M_D_IMG_OG","assets/images/meta/index/og.jpg"); // Default page
define("M_D_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Default page
define("M_D_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Default page

// ABOUT US
define("M_ABOUT_TITLE","Sri Lanka Karting Circuit Bandaragama | "); // Title
define("M_ABOUT_DESC","Sri Lanka Karting Circuit Bandaragama | "); // Description
define("M_ABOUT_IMG","assets/images/meta/index/web_img.png"); // Sharing Image
define("M_ABOUT_IMG_OG","assets/images/meta/index/og.jpg"); // Facebook Image
define("M_ABOUT_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Twitter Image
define("M_ABOUT_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Linked In Image

// Contact
define("M_CONTACT_TITLE","Contact Details Of karting Circuit | "); // Title
define("M_CONTACT_DESC","Sri Lanka Karting Contact Details | "); // Description
define("M_CONTACT_IMG","assets/images/meta/index/web_img.png"); // Sharing Image
define("M_CONTACT_IMG_OG","assets/images/meta/index/og.jpg"); // Facebook Image
define("M_CONTACT_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Twitter Image
define("M_CONTACT_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Linked In Image

// House Rules Informations
define("M_HOUSE_RULES_TITLE","House Rules Informations | "); // Title
define("M_HOUSE_RULES_DESC","House Rules Informaions of Sri Lanka Karting Circuit | "); // Description
define("M_HOUSE_RULES_IMG","assets/images/meta/index/web_img.png"); // Sharing Image
define("M_HOUSE_RULES_IMG_OG","assets/images/meta/index/og.jpg"); // Facebook Image
define("M_HOUSE_RULES_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Twitter Image
define("M_HOUSE_RULES_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Linked In Image

// Shooting Range
define("M_SHOOTING_TITLE","Shooting Range | "); // Title
define("M_SHOOTING_DESC","Shooting Range | David Pieris Leisure Center | "); // Description
define("M_SHOOTING_IMG","assets/images/meta/index/web_img.png"); // Sharing Image
define("M_SHOOTING_IMG_OG","assets/images/meta/index/og.jpg"); // Facebook Image
define("M_SHOOTING_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Twitter Image
define("M_SHOOTING_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Linked In Image

// Paddock Restaurant
define("M_PADDOCK_TITLE","Paddock Restaurant | "); // Title
define("M_PADDOCK_DESC","Paddock Restaurant | David Pieris Leisure Center | "); // Description
define("M_PADDOCK_IMG","assets/images/meta/index/web_img.png"); // Sharing Image
define("M_PADDOCK_IMG_OG","assets/images/meta/index/og.jpg"); // Facebook Image
define("M_PADDOCK_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Twitter Image
define("M_PADDOCK_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Linked In Image

// Swimming Pool
define("M_SWIMMING_TITLE","Swimming Pool | "); // Title
define("M_SWIMMING_DESC","Swimming Pool | David Pieris Leisure Center | "); // Description
define("M_SWIMMING_IMG","assets/images/meta/index/web_img.png"); // Sharing Image
define("M_SWIMMING_IMG_OG","assets/images/meta/index/og.jpg"); // Facebook Image
define("M_SWIMMING_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Twitter Image
define("M_SWIMMING_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Linked In Image

// Photos
define("M_PHOTOS_TITLE","Photos of SL Karting | SL Karting Gallery | "); // Title
define("M_PHOTOS_DESC","Photos of SL Karting | SL Karting Gallery | "); // Description
define("M_PHOTOS_IMG","assets/images/meta/index/web_img.png"); // Sharing Image
define("M_PHOTOS_IMG_OG","assets/images/meta/index/og.jpg"); // Facebook Image
define("M_PHOTOS_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Twitter Image
define("M_PHOTOS_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Linked In Image

// Videos
define("M_VIDEOS_TITLE","Videos of SL Karting | SL Karting Video Gallery | "); // Title
define("M_VIDEOS_DESC","Videos of SL Karting | SL Karting Video Gallery | "); // Description
define("M_VIDEOS_IMG","assets/images/meta/index/web_img.png"); // Sharing Image
define("M_VIDEOS_IMG_OG","assets/images/meta/index/og.jpg"); // Facebook Image
define("M_VIDEOS_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Twitter Image
define("M_VIDEOS_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Linked In Image

// Driving Experience Videos
define("M_DRIVING_EXP_TITLE","Karting Driving Experience Videos | "); // Title
define("M_DRIVING_EXP_DESC","Karting Driving Experience Videos | "); // Description
define("M_DRIVING_EXP_IMG","assets/images/meta/index/web_img.png"); // Sharing Image
define("M_DRIVING_EXP_IMG_OG","assets/images/meta/index/og.jpg"); // Facebook Image
define("M_DRIVING_EXP_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Twitter Image
define("M_DRIVING_EXP_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Linked In Image

// Text Reviews
define("M_TEXT_REVIEWS_TITLE","Text Reviews | "); // Title
define("M_TEXT_REVIEWS_DESC","Customers Text Reviews Of karting | "); // Description
define("M_TEXT_REVIEWS_IMG","assets/images/meta/index/web_img.png"); // Sharing Image
define("M_TEXT_REVIEWS_IMG_OG","assets/images/meta/index/og.jpg"); // Facebook Image
define("M_TEXT_REVIEWS_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Twitter Image
define("M_TEXT_REVIEWS_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Linked In Image

// Video Reviews
define("M_VIDEO_REVIEWS_TITLE","Videos Reviews | "); // Title
define("M_VIDEO_REVIEWS_DESC","Customers Videos Reviews Karting | "); // Description
define("M_VIDEO_REVIEWS_IMG","assets/images/meta/index/web_img.png"); // Sharing Image
define("M_VIDEO_REVIEWS_IMG_OG","assets/images/meta/index/og.jpg"); // Facebook Image
define("M_VIDEO_REVIEWS_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Twitter Image
define("M_VIDEO_REVIEWS_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Linked In Image



// KARTING INSIDE PAGE
// Circuit
define("M_K_CIRCUIT_TITLE","Bandaragama Karting Circuit, Circuit layouts and specifications | "); // Title
define("M_K_CIRCUIT_DESC","Bandaragama Karting Circuit, Circuit layouts and specifications | "); // Description
define("M_K_CIRCUIT_IMG","assets/images/meta/index/web_img.png"); // Sharing Image
define("M_K_CIRCUIT_IMG_OG","assets/images/meta/index/og.jpg"); // Facebook Image
define("M_K_CIRCUIT_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Twitter Image
define("M_K_CIRCUIT_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Linked In Image

// Rates
define("M_RATES_TITLE","SL karting latest rates and offers | "); // Title
define("M_RATES_DESC","SL karting latest rates and offers | "); // Description
define("M_RATES_IMG","assets/images/meta/index/web_img.png"); // Sharing Image
define("M_RATES_IMG_OG","assets/images/meta/index/og.jpg"); // Facebook Image
define("M_RATES_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Twitter Image
define("M_RATES_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Linked In Image

// Types of karts
define("M_TYPES_OF_KARTS_TITLE","Types of karts in SL Karting | "); // Title
define("M_TYPES_OF_KARTS_DESC","Types of karts in SL Karting | "); // Description
define("M_TYPES_OF_KARTS_IMG","assets/images/meta/index/web_img.png"); // Sharing Image
define("M_TYPES_OF_KARTS_IMG_OG","assets/images/meta/index/og.jpg"); // Facebook Image
define("M_TYPES_OF_KARTS_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Twitter Image
define("M_TYPES_OF_KARTS_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Linked In Image




// NEWS ANA EVENTS
// News & events index
define("M_NEWS_EVENTS_TITLE","News and Events of Karting in Sri Lanka | "); // Title
define("M_NEWS_EVENTS_DESC","News and Events of Karting in Sri Lanka | "); // Description
define("M_NEWS_EVENTS_IMG","assets/images/meta/index/web_img.png"); // Sharing Image
define("M_NEWS_EVENTS_IMG_OG","assets/images/meta/index/og.jpg"); // Facebook Image
define("M_NEWS_EVENTS_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Twitter Image
define("M_NEWS_EVENTS_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Linked In Image



// BLOG
// Blog index
define("M_BLOG_TITLE","Sri Lanka Karting Blog | "); // Title
define("M_BLOG_DESC","Sri Lanka Karting Blog | "); // Description
define("M_BLOG_IMG","assets/images/meta/index/web_img.png"); // Sharing Image
define("M_BLOG_IMG_OG","assets/images/meta/index/og.jpg"); // Facebook Image
define("M_BLOG_IMG_TWITTER","assets/images/meta/index/twitter.jpg"); // Twitter Image
define("M_BLOG_IMG_ITEMSCOPE","assets/images/meta/index/itemscope.jpg"); // Linked In Image

?>