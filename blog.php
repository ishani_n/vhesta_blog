<?php 
    require_once 'app/global/url.php';
  
?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

<?php 
        require_once ROOT_PATH.'app/meta/meta.php';
        $meta_single_page_title = 'vhesta blog';
        $meta_single_page_desc = '';
        $meta_arr = array(
            'title' => $meta_single_page_title,
            'description' => $meta_single_page_desc,
            'image' => URL.'assets/images/meta/home.jpg',
            
            'og:title' => $meta_single_page_title,
            'og:image' => URL.'assets/images/meta/home.jpg',
            'og:description' => $meta_single_page_desc,

            'twitter:image' => URL.'assets/images/meta/home.jpg',
            'twitter:title' => $meta_single_page_title,

        );
        require_once ROOT_PATH.'app/meta/meta_more_details.php'; 
    ?>
	<?php include_once ROOT_PATH.'imports/css.php'; ?>


	<!-- Document Title
	============================================= -->
	<title>Blog Single | Canvas</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

	

		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Blog Single</h1>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Blog</a></li>
					<li class="breadcrumb-item active" aria-current="page">Blog Single</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content" style="width:100%;margin-top: 0px;">
			<div class="content-wrap">
				<div class="container clearfix">

					<div class="row gutter-40 col-mb-80">
						<!-- Post Content
						============================================= -->
						<div class="postcontent col-lg-9">

							<div class="single-post mb-0">

								<!-- Single Post
								============================================= -->
								<div class="entry clearfix">

									<!-- Entry Title
									============================================= -->
									<div class="entry-title">
										<h2>This is a Standard post with a Preview Image</h2>
									</div><!-- .entry-title end -->

									<!-- Entry Meta
									============================================= -->
									<div class="entry-meta">
										<ul>
											<li><i class="far fa-calendar-alt"></i> 10th July 2021</li>
											<li><i class="fas fa-map-marker-alt"></i> <a href="#">General</a>, <a href="#">Media</a></li>
										</ul>
									</div><!-- .entry-meta end -->

									<!-- Entry Image
									============================================= -->
									<div class="entry-image">
										<a href="#"><img src="<?php echo URL?>assets/img/blog1.jpg" alt="Blog Single"></a>
									</div><!-- .entry-image end -->

									<!-- Entry Content
									============================================= -->
									<div class="entry-content mt-0">

										<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>

										<p>Nullam id dolor id nibh ultricies vehicula ut id elit. <a href="#">Curabitur blandit tempus porttitor</a>. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Donec id elit non mi porta gravida at eget metus. Vestibulum id ligula porta felis euismod semper.</p>

										<blockquote><p>Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper.</p></blockquote>

										<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras mattis consectetur purus sit amet fermentum. Donec id elit non mi porta gravida at eget metus.</p>

										<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consectetur. Cras justo odio, dapibus ac facilisis in, egestas eget quam. <a href="#">Nullam quis risus eget urna</a> mollis ornare vel eu leo. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>


										<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>

										<p>Nullam id dolor id nibh ultricies vehicula ut id elit. Curabitur blandit tempus porttitor. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Donec id elit non mi porta gravida at eget metus. Vestibulum id ligula porta felis euismod semper.</p>
										<!-- Post Single - Content End -->

										<!-- Tag Cloud
										============================================= -->
										<div class="tagcloud clearfix bottommargin">
											<a href="#">general</a>
											<a href="#">information</a>
											<a href="#">media</a>
											<a href="#">press</a>
											<a href="#">gallery</a>
											<a href="#">illustration</a>
										</div><!-- .tagcloud end -->

										<div class="clear"></div>

										<!-- Post Single - Share
										============================================= -->
										<!-- <div class="si-share border-0 d-flex justify-content-between align-items-center">
											<span>Share this Post:</span>
											<div>
												<a href="#" class="social-icon si-borderless si-facebook">
													<i class="fab fa-facebook-f"></i>
													
												</a>
												<a href="#" class="social-icon si-borderless si-twitter">
													<i class="icon-twitter"></i>
													<i class="icon-twitter"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-pinterest">
													<i class="icon-pinterest"></i>
													<i class="icon-pinterest"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-gplus">
													<i class="icon-gplus"></i>
													<i class="icon-gplus"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-rss">
													<i class="icon-rss"></i>
													<i class="icon-rss"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-email3">
													<i class="icon-email3"></i>
													<i class="icon-email3"></i>
												</a>
											</div>
										</div> -->

									</div>
								</div><!-- .entry end -->

							

								

								

								<div class="line"></div>

								<h4>Related Posts:</h4>

								<div class="related-posts row posts-md col-mb-30">

									<div class="entry col-12 col-md-6">
										<div class="grid-inner row align-items-center gutter-20">
											<div class="col-4">
												<div class="entry-image">
													<a href="#"><img src="<?php echo URL?>assets/img/10.jpg" alt="Blog Single"></a>
												</div>
											</div>
											<div class="col-8">
												<div class="entry-title title-xs">
													<h3><a href="#">This is an Image Post</a></h3>
												</div>
												<div class="entry-meta">
													<ul>
														<li><i class="far fa-calendar-alt"></i> 10th July 2021</li>
													</ul>
												</div>
											</div>
										</div>
									</div>

									<div class="entry col-12 col-md-6">
										<div class="grid-inner row align-items-center gutter-20">
											<div class="col-4">
												<div class="entry-image">
													<a href="#"><img src="<?php echo URL?>assets/img/10.jpg" alt="Blog Single"></a>
												</div>
											</div>
											<div class="col-8">
												<div class="entry-title title-xs">
													<h3><a href="#">This is a Video Post</a></h3>
												</div>
												<div class="entry-meta">
													<ul>
														<li><i class="far fa-calendar-alt"></i> 24th July 2021</li>
													</ul>
												</div>
											</div>
										</div>
									</div>

									<div class="entry col-12 col-md-6">
										<div class="grid-inner row align-items-center gutter-20">
											<div class="col-4">
												<div class="entry-image">
													<a href="#"><img src="<?php echo URL?>assets/img/10.jpg" alt="Blog Single"></a>
												</div>
											</div>
											<div class="col-8">
												<div class="entry-title title-xs">
													<h3><a href="#">This is a Gallery Post</a></h3>
												</div>
												<div class="entry-meta">
													<ul>
														<li><i class="far fa-calendar-alt"></i> 8th Aug 2021</li>
													</ul>
												</div>
											</div>
										</div>
									</div>

									<div class="entry col-12 col-md-6">
										<div class="grid-inner row align-items-center gutter-20">
											<div class="col-4">
												<div class="entry-image">
													<a href="#"><img src="<?php echo URL?>assets/img/10.jpg" alt="Blog Single"></a>
												</div>
											</div>
											<div class="col-8">
												<div class="entry-title title-xs">
													<h3><a href="#">This is an Audio Post</a></h3>
												</div>
												<div class="entry-meta">
													<ul>
														<li><i class="far fa-calendar-alt"></i> 22nd Aug 2021</li>
													</ul>
												</div>
											</div>
										</div>
									</div>

								</div>

								

							</div>

						</div><!-- .postcontent end -->

						<!-- Sidebar
						============================================= -->
						<div class="sidebar col-lg-3">
							<div class="sidebar-widgets-wrap">

							


								<div class="widget clearfix">

									<div class="tabs mb-0 clearfix" id="sidebar-tabs">

										<ul class="tab-nav clearfix">
											<li><a href="#tabs-1">Popular</a></li>
											<li><a href="#tabs-2">Recent</a></li>
											
										</ul>

										<div class="tab-container">

											<div class="tab-content clearfix" id="tabs-1">
												<div class="posts-sm row col-mb-30" id="popular-post-list-sidebar">
													<div class="entry col-12">
														<div class="grid-inner row no-gutters">
															<div class="col-auto">
																<div class="entry-image">
																	<a href="#"><img class="rounded-circle" src="<?php echo URL?>assets/img/pop-img.jpg" alt="Image"></a>
																</div>
															</div>
															<div class="col pl-3">
																<div class="entry-title">
																	<h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
																</div>
																
															</div>
														</div>
													</div>

													<div class="entry col-12">
														<div class="grid-inner row no-gutters">
															<div class="col-auto">
																<div class="entry-image">
																	<a href="#"><img class="rounded-circle" src="<?php echo URL?>assets/img/pop-img.jpg" alt="Image"></a>
																</div>
															</div>
															<div class="col pl-3">
																<div class="entry-title">
																	<h4><a href="#">Elit Assumenda vel amet dolorum quasi</a></h4>
																</div>
																
															</div>
														</div>
													</div>

													<div class="entry col-12">
														<div class="grid-inner row no-gutters">
															<div class="col-auto">
																<div class="entry-image">
																	<a href="#"><img class="rounded-circle" src="<?php echo URL?>assets/img/pop-img.jpg" alt="Image"></a>
																</div>
															</div>
															<div class="col pl-3">
																<div class="entry-title">
																	<h4><a href="#">Debitis nihil placeat, illum est nisi</a></h4>
																</div>
																
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tab-content clearfix" id="tabs-2">
												<div class="posts-sm row col-mb-30" id="recent-post-list-sidebar">
													<div class="entry col-12">
														<div class="grid-inner row no-gutters">
															<div class="col-auto">
																<div class="entry-image">
																	<a href="#"><img class="rounded-circle" src="<?php echo URL?>assets/img/pop-img.jpg" alt="Image"></a>
																</div>
															</div>
															<div class="col pl-3">
																<div class="entry-title">
																	<h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
																</div>
																<div class="entry-meta">
																	<ul>
																		<li>10th July 2021</li>
																	</ul>
																</div>
															</div>
														</div>
													</div>

													<div class="entry col-12">
														<div class="grid-inner row no-gutters">
															<div class="col-auto">
																<div class="entry-image">
																	<a href="#"><img class="rounded-circle" src="<?php echo URL?>assets/img/pop-img.jpg" alt="Image"></a>
																</div>
															</div>
															<div class="col pl-3">
																<div class="entry-title">
																	<h4><a href="#">Elit Assumenda vel amet dolorum quasi</a></h4>
																</div>
																<div class="entry-meta">
																	<ul>
																		<li>10th July 2021</li>
																	</ul>
																</div>
															</div>
														</div>
													</div>

													<div class="entry col-12">
														<div class="grid-inner row no-gutters">
															<div class="col-auto">
																<div class="entry-image">
																	<a href="#"><img class="rounded-circle" src="<?php echo URL?>assets/img/pop-img.jpg" alt="Image"></a>
																</div>
															</div>
															<div class="col pl-3">
																<div class="entry-title">
																	<h4><a href="#">Debitis nihil placeat, illum est nisi</a></h4>
																</div>
																<div class="entry-meta">
																	<ul>
																		<li>10th July 2021</li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										

										</div>

									</div>

								</div>

								<div class="widget clearfix">

									<h4>Portfolio Carousel</h4>
									<div id="oc-portfolio-sidebar" class="owl-carousel carousel-widget" data-items="1" data-margin="10" data-loop="true" data-nav="false" data-autoplay="5000">

										<div class="oc-item">
											<div class="portfolio-item">
												<div class="portfolio-image">
													<a href="portfolio-single.html">
														<img src="<?php echo URL?>assets/img/blog1.jpg" alt="Open Imagination">
													</a>
												</div>
												<div class="portfolio-desc text-center pb-0">
													<h3><a href="portfolio-single.html">Open Imagination</a></h3>
													<span><a href="#">Media</a>, <a href="#">Icons</a></span>
												</div>
											</div>
										</div>

										<div class="oc-item">
											<div class="portfolio-item">
												<div class="portfolio-image">
													<a href="portfolio-single.html">
														<img src="<?php echo URL?>assets/img/blog1.jpg" alt="Open Imagination">
													</a>
												</div>
												<div class="portfolio-desc text-center pb-0">
													<h3><a href="portfolio-single.html">Open Imagination</a></h3>
													<span><a href="#">Media</a>, <a href="#">Icons</a></span>
												</div>
											</div>
										</div>

									</div>


								</div>

								<div class="widget clearfix">

									<h4>Tag Cloud</h4>
									<div class="tagcloud">
										<a href="#">general</a>
										<a href="#">videos</a>
										<a href="#">music</a>
										<a href="#">media</a>
										<a href="#">photography</a>
										<a href="#">parallax</a>
										<a href="#">ecommerce</a>
										<a href="#">terms</a>
										<a href="#">coupons</a>
										<a href="#">modern</a>
									</div>

								</div>

							</div>
						</div><!-- .sidebar end -->
					</div>

				</div>
			</div>
		</section><!-- #content end -->

		

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
    <?php include_once ROOT_PATH.'imports/js.php'; ?>


</body>
</html>
