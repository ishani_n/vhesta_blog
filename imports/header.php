<!-- Header
		============================================= -->
		<header id="header" class="transparent-header header-size-md" data-sticky-shrink="false">
			<div id="header-wrap">
				<div class="container-fluid">
					<div class="header-row">

						<!-- Logo
						============================================= -->
						<div id="logo">
							<a href="index.html" class="standard-logo" data-mobile-logo="images/logo.png"><img src="<?php echo URL?>assets/img/logo.PNG" alt="Canvas Logo"></a>
							<a href="index.html" class="retina-logo" data-mobile-logo="images/logo@2x.png"><img src="<?php echo URL?>assets/img/logo.PNG" alt="Canvas Logo"></a>
						</div><!-- #logo end -->

						<!-- <div id="primary-menu-trigger">
							<svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
						</div> -->

						<!-- Primary Navigation
						============================================= -->
						<!-- <nav class="primary-menu text-lg-center">
							<ul class="menu-container">
								<li class="menu-item current"><a class="menu-link" href="#"><div>Home</div></a></li>
								<li class="menu-item"><a class="menu-link" href="#"><div>Articles</div></a></li>
								<li class="menu-item"><a class="menu-link" href="#"><div>Events</div></a></li>
								<li class="menu-item"><a class="menu-link" href="#"><div>Videos</div></a></li>
								<li class="menu-item"><a class="menu-link" href="#"><div>Contact</div></a></li>
							</ul>
						</nav> -->
                        <!-- #primary-menu end -->

					</div>
				</div>
			</div>
			<div class="header-wrap-clone"></div>
		</header><!-- #header end -->
