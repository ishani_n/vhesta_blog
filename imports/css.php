<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo URL?>assets/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo URL?>assets/css/style.css" type="text/css" />
	<!-- <link rel="stylesheet" href="<?php echo URL?>assets/css/dark.css" type="text/css" /> -->

	<link rel="stylesheet" href="<?php echo URL?>assets/css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo URL?>assets/one-page/css/et-line.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo URL?>assets/css/animate.css" type="text/css" />
	<!-- <link rel="stylesheet" href="<?php echo URL?>assets/css/magnific-popup.css" type="text/css" /> -->

	<link rel="stylesheet" href="<?php echo URL?>assets/css/style_new.css?v=<?php echo VERSION ?>2" type="text/css" />


	<!-- <link rel="stylesheet" href="<?php echo URL?>assets/css/custom.css" type="text/css" /> -->
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Modern Blog Demo Specific Stylesheet -->
	<!-- <link rel="stylesheet" href="<?php echo URL?>assets/css/modern-blog.css" type="text/css" /> -->
	<link rel="stylesheet" href="demos/modern-blog/css/font-icons.css" type="text/css" />
	<!-- <link rel="stylesheet" href="<?php echo URL?>assets/css/colors.php?color=dc3545" type="text/css" /> -->
	<!-- / -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />
	<!-- fonts -->
	<link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,600;0,700;1,200;1,300;1,700&display=swap" rel="stylesheet">
	<!-- font-family: 'Nunito', sans-serif; -->
	<link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
	<!-- font-family: 'Work Sans', sans-serif; -->